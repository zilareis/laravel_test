@extends('adminlte::page')

@section('content')
<div class="container">
    <table class="table table-striped">
        <thead>
            <tr>
                <td>ID</td>
                <td>Title</td>
                <td>Description</td>
                <td>Email</td>
                <td colspan="2">Action</td>
            </tr>
        </thead>
        <tbody>
            @foreach($tickets as $ticket)
            <tr>
                <td>{{$ticket->ticket_id}}</td>
                <td>{{$ticket->title}}</td>
                <td>{{$ticket->description}}</td>
                <td>{{$ticket->email}}</td>
                <td><a href="{{action('TicketController@edit',$ticket->ticket_id)}}" class="btn btn-primary">Edit</a></td>
                <td><a href="{{action('TicketController@show',$ticket->ticket_id)}}" class="btn btn-primary">Detail</a></td>
                <td>
                    <form action="{{action('TicketController@destroy', $ticket->ticket_id)}}" method="post">
                    {{csrf_field()}}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $tickets->links() }}
<div>
@endsection

